import 'dart:math';

import 'package:drone/animated_drone.dart';
import 'package:drone/drone.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  Drone selectedDrone = drones.first;

  @override
  Widget build(BuildContext context) {
    var intValue = Random().nextInt(100) + 50;
    return SafeArea(
      child: Scaffold(
        floatingActionButton: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FloatingActionButton(
            onPressed: (){
              for (var drone in drones) {
                drone.rValue = 0;
                drone.oValue = 0;
              }
              setState(() {});
              _sendSelectedDroneInformation();
            },
            backgroundColor: Colors.orange,
            child: const Icon(Icons.replay_circle_filled,color: Colors.white,size: 35,),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.miniEndTop,
        body: Column(
          children: [
            Expanded(
              flex: 6,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    const Expanded(
                      flex: 2,
                      child: Center(
                        child: Text(
                          "CENTER OF \nEARTH",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 28),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 8,
                      child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Colors.orange,
                              width: 4,
                            ),
                          ),
                          child: Center(
                              child: Stack(
                            children: [

                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.15,
                                top: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.1,
                                child: Container(
                                  height: MediaQuery.of(context).size.width * 0.1,
                                  width: MediaQuery.of(context).size.width * 0.15,
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black ,width: 1)
                                  ),
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24)- MediaQuery.of(context).size.width * 0.15 - drones[0].rValue,
                                top: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.1 - drones[0].rValue,
                                child: CircularElement(animation: AnimationController(
                                  vsync: this,
                                  duration: Duration(seconds: 2), // You can adjust the duration here
                                )..repeat(),
                                  drone: drones[0],
                                  isClockWise: false,
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) ,
                                top: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.1,
                                child: Container(
                                  height: MediaQuery.of(context).size.width * 0.1,
                                  width: MediaQuery.of(context).size.width * 0.15,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black ,width: 1)
                                  ),
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) + drones[1].rValue,
                                top: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.1 - drones[1].rValue,
                                child: CircularElement(animation: AnimationController(
                                  vsync: this,
                                  duration: Duration(seconds: 2), // You can adjust the duration here
                                )..repeat(),
                                  drone: drones[1],
                                ),
                              ),

                              Positioned(
                                left:(MediaQuery.of(context).size.width /2 - 24)- MediaQuery.of(context).size.width * 0.15,
                                top: (MediaQuery.of(context).size.width /2 - 24) ,
                                child: Container(
                                  height: MediaQuery.of(context).size.width * 0.1,
                                  width: MediaQuery.of(context).size.width * 0.15,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black ,width: 1)
                                  ),
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) - MediaQuery.of(context).size.width * 0.15 - drones[2].rValue,
                                top: (MediaQuery.of(context).size.width /2 - 24) + drones[2].rValue,
                                child: CircularElement(animation: AnimationController(
                                  vsync: this,
                                  duration: Duration(seconds: 2), // You can adjust the duration here
                                )..repeat(),
                                  drone: drones[2],
                                  isClockWise: false,
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) ,
                                top: (MediaQuery.of(context).size.width /2 - 24),
                                child: Container(
                                  height: MediaQuery.of(context).size.width * 0.1,
                                  width: MediaQuery.of(context).size.width * 0.15,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.black ,width: 1)
                                  ),
                                ),
                              ),
                              Positioned(
                                left: (MediaQuery.of(context).size.width /2 - 24) + drones[3].rValue,
                                top: (MediaQuery.of(context).size.width /2 - 24) + drones[3].rValue,
                                child: CircularElement(animation: AnimationController(
                                  vsync: this,
                                  duration: Duration(seconds: 2), // You can adjust the duration here
                                )..repeat(),
                                  drone: drones[3],
                                ),
                              ),
                            ],
                          )),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Container(
                color: Colors.white,
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "R",
                          style: TextStyle(fontSize: 24),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: SliderTheme(
                            data: const SliderThemeData(
                                thumbShape: RoundSliderThumbShape(
                                    enabledThumbRadius: 5),
                                trackHeight: 3),
                            child: Slider(
                              max: 100,
                              min: 0,
                              activeColor: Colors.orange,
                              value: selectedDrone.rValue,
                              onChanged: (value) {
                                setState(() {
                                  selectedDrone.rValue = value;
                                  if(selectedDrone.rValue < 30) {
                                    selectedDrone.oValue = 0;
                                  }
                                });
                              },
                              onChangeEnd: (value){
                                _sendSelectedDroneInformation();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          "O",
                          style: TextStyle(fontSize: 24),
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.9,
                          child: SliderTheme(
                              data: const SliderThemeData(
                                  thumbShape: RoundSliderThumbShape(
                                      enabledThumbRadius: 5),
                                  trackHeight: 3),
                              child: Slider(
                                min: 0,
                                max: 5,
                                activeColor: Colors.orange,
                                value: selectedDrone.oValue,
                                onChanged: selectedDrone.rValue < 29? null: (value) {
                                  setState(() {
                                    selectedDrone.oValue = value;
                                  });
                                },
                                onChangeEnd: (value){
                                  _sendSelectedDroneInformation();
                                },
                              )),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0),
                      child: SizedBox(
                        height: 100,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: Container(
                                    color: Colors.grey,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Di",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        DropdownButton(
                                          iconEnabledColor: Colors.orange,
                                            items: drones
                                                .map((Drone d) =>
                                                    DropdownMenuItem(
                                                      value: d,
                                                        child: Text(
                                                      d.name,
                                                      style: const TextStyle(
                                                          fontSize: 20),
                                                    )))
                                                .toList(),
                                            value: selectedDrone,
                                            onChanged: (Drone? d) {
                                              selectedDrone = d!;
                                              setState(() {});
                                            })
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: Container(
                                    color: Colors.grey,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Ri",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        Text(
                                          selectedDrone.rValue.round().toString(),
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                        const Text(
                                          "m",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: Container(
                                    color: Colors.grey,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Oi",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        Text(
                                          selectedDrone.oValue.round().toString(),
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                        const Text(
                                          "m",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 1,
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 2.0),
                                  child: Container(
                                    color: Colors.grey,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Si",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        Text(
                                          intValue.toString(),
                                          style: const TextStyle(fontSize: 20),
                                        ),
                                        const Text(
                                          "°C",
                                          style: TextStyle(fontSize: 20),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _sendSelectedDroneInformation() {
    /// replace this code with your implementation to send drones information ;)
    if (kDebugMode) {
      print(selectedDrone.toString());
    }
  }
}
