import 'package:flutter/material.dart';

class Drone {
  String name;
  double oValue;
  double rValue;
  Image image;

  Drone(
      {this.oValue = 0,
      this.rValue = 0,
      required this.image,
      required this.name});

  @override
  String toString() {
    return 'Drone{name: $name, oValue: $oValue, rValue: $rValue}';
  }
}

List<Drone> drones = [
  Drone(
    image: Image.asset("assets/images/drone.png"),
    name: "D1",
  ),
  Drone(
    image: Image.asset("assets/images/drone.png"),
    name: "D2",
  ),
  Drone(
    image: Image.asset("assets/images/drone.png"),
    name: "D3",
  ),
  Drone(
    image: Image.asset("assets/images/drone.png"),
    name: "D4",
  ),
];
