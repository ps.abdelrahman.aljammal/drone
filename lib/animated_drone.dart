import 'dart:math';

import 'package:flutter/material.dart';

import 'drone.dart';

class CircularElement extends AnimatedWidget {
  final Animation<double> animation;
  Drone drone;
  bool isClockWise;

  CircularElement(
      {super.key,
      required this.animation,
      required this.drone,
      this.isClockWise = true})
      : super(listenable: animation);

  @override
  Widget build(BuildContext context) {
    double radius =
        drone.oValue * 5; // Adjust the radius as per your requirement

    // Calculate the position of the element in the circular path
    double x = radius *cos(animation.value * 2 * pi);
    double y = radius *sin(animation.value * 2 * pi);

    return Transform.translate(
      offset: isClockWise ? Offset(x, y) : Offset(y, x),
      child: SizedBox(
        height: MediaQuery.of(context).size.width * 0.1,
        width: MediaQuery.of(context).size.width * 0.15,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: drone.image,
        ),
      ),
    );
  }
}
